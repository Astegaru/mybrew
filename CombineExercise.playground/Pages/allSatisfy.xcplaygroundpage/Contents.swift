//: [Previous](@previous)

import Foundation
import Combine

let publisher = ["A","B","C","D",""].publisher

publisher
    .allSatisfy { !$0.isEmpty}
    .sink {
        print($0)
    }


//: [Next](@next)
