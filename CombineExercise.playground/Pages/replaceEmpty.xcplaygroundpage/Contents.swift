//: [Previous](@previous)

import Foundation
import Combine


let empty = Empty<Int,Never>()
empty
    .replaceEmpty(with: 1)
    .sink {
    print($0)
} receiveValue: {
    print($0)
}


//: [Next](@next)
