//: [Previous](@previous)

import Foundation
import Combine

// MARK: - With Keypath

struct Point {
    let x: Int
    let y: Int
}

let publisher = PassthroughSubject<Point,Never>()

publisher
    .map(\.x, \.y)
    .sink { (x, y) in
        print("X is \(x) and Y is: \(y)")
    }
publisher.send(Point(x: 23, y: 45))
publisher.send(Point(x: 52, y: 31))

// MARK: - Without Keypath
print("\n Without Keypath \n")
let pulisher = PassthroughSubject<Point,Never>()
pulisher
    .map { point -> (Int,  Int) in
        return (point.x, point.y)
    }
    .sink { (x, y) in
        print("X is \(x) and Y is: \(y)")
    }
publisher.send(Point(x: 23, y: 45))
publisher.send(Point(x: 52, y: 31))

//: [Next](@next)
