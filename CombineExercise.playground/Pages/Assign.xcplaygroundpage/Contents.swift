//: [Previous](@previous)

import Foundation
import UIKit
import Combine

class SomeClass: ObservableObject {
    var someVar: Int = 0
//    @Published var someNumber: Int = 1
    
    init() {
        print(someVar)
        Just(10)
            .assign(to: \.someVar, on: self)
            .cancel()
        print(someVar)
        
    }
}

let someClass = SomeClass()

// Mark: - Assign

// 1. Creating notification
let notification = Notification.Name("SomeNotification")
// 2. Creating a publisher for notification center that we can subsribe to later on
let publisher = NotificationCenter.default.publisher(for: notification, object: nil)
// 3. Create notification center post in order to post the notificaiton. This wont get sent because we are posting before having a subscriber to our publisher, we will post it again on step 5
NotificationCenter.default.post(name: notification, object: nil)
// 4. Creating a label where we will asign the notification stuff
let lastPostLable = UILabel()
// 5. mapping the notification object to get it's title as a string
let mappedPublisher = publisher.map { (notification) -> String? in
    return notification.object as? String ?? "No valid title"
}
// MARK: 6. Assigning the mappedPublisher (in our case it's text that we computed to the label's text, this will change upon posting notifications objects under the same publisher
mappedPublisher.assign(to: \.text, on: lastPostLable)
    
// 7. Creating another post from our Notificaiton Center after adding a subscription to our publisher in order to see the print from sink
NotificationCenter.default.post(name: notification, object: 7)
print(lastPostLable.text!)
NotificationCenter.default.post(name: notification, object: "Alexandru stie notifications cu combine")
print(lastPostLable.text!)
NotificationCenter.default.post(name: notification, object: "AAAAAA")
print(lastPostLable.text!)
// 8. You can see above that the we subscribed the label's text to the publisher with the assign.

//: [Next](@next)
