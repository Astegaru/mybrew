//: [Previous](@previous)

import Foundation
import Combine

// Two different variations

let publisher = ["A","B","C","D"].publisher
publisher.output(at: 2)

    .sink {
        print($0)
    }

publisher.output(in: (0...4))
    .sink {
        print($0)
    }




//: [Next](@next)
