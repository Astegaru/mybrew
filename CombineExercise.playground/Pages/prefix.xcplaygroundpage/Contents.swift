//: [Previous](@previous)

import Foundation
import Combine

//Prefix

let numbers = (1...10).publisher

numbers.prefix(2).sink {print($0)}

// While

let numbers2 = (1...10).publisher
numbers2.prefix(while: {$0 < 5})
    .sink {
        print($0)
    }


//: [Next](@next)
