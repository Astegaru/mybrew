//: [Previous](@previous)

import Foundation
import Combine
// Flatten multiple upstream publishers into a single downstream publisher

struct School {
    let name: String
    let noOfStudents: CurrentValueSubject<Int, Never>
    
    init(name: String, noOfStudents: Int) {
        self.name = name
        self.noOfStudents = CurrentValueSubject(noOfStudents)
    }
}

let citySchool = School(name: "Some Stupid Schook", noOfStudents: 100)
let school = CurrentValueSubject<School,Never>(citySchool)

school.sink {
    print($0)
}

//: [Next](@next)
