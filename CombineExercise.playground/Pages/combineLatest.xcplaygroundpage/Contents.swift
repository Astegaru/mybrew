//: [Previous](@previous)

import Foundation
import Combine

// COmbine two different publsihers of different types and it's going to give you a tuple of values from your publishers

let publisher1 = PassthroughSubject<Int,Never>()

let publisher2 = PassthroughSubject<String,Never>()

publisher1.combineLatest(publisher2)
    .sink {
        print($0.0, $0.1)
    }

publisher1.send(1)
publisher2.send("Yas")

publisher2.send("B")
//: [Next](@next)
