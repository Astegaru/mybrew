//: [Previous](@previous)

import Foundation
import Combine

//

let words = "apple apple fruit apple apple mango watermelon apple".components(separatedBy: " ").publisher // amkes an aarray
words
    .removeDuplicates()
    .sink {
    print($0)
}

// It looks at the sequence and removes consecutive values if they are duplicates of a value in the stream. To note that it gets them one by one and that's why apple is not removed after fruit or watermelon (only the duplicate)
//: [Next](@next)
