//: [Previous](@previous)

import Foundation

// MARK: - Prequisites
let formatter = NumberFormatter()
formatter.numberStyle = .spellOut
// MARK: - Simple version with sink
[21,1994,01].publisher
    .map {
        formatter.string(from: NSNumber(integerLiteral: $0)) ?? ""
    }
    .sink { print($0)
    }


// MARK: - Making use of map as intended
[123,45,65].publisher.map { value -> String in
    let formattedNumber = formatter.string(from: NSNumber(integerLiteral: value)) ?? ""
    print(formattedNumber)
    return formattedNumber
}

//: [Next](@next)
