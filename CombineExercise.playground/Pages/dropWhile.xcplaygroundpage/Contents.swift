//: [Previous](@previous)

import Foundation
import Combine

// DropWhile, it's going to drop values while the condition is true.

let numbers = (4...10).publisher
    .drop(while: {$0 % 3 != 0})
    .sink {
        print($0)
    }


//: [Next](@next)
