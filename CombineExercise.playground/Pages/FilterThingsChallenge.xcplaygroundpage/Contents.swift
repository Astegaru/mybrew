//: [Previous](@previous)

import Foundation
import Combine

let numbers = (1...100).publisher

numbers
    .dropFirst(50)
    .prefix(20)
    .filter {
        $0 % 2 == 0
    }
    .sink{
        print($0)
    }

// Better solution, one liner
print("\n One liner \n")
let numbers2 = (1...100).publisher
numbers2.filter { numbers in
    numbers % 2 == 0 && numbers > 50 && numbers < 71
}
.sink {
    print($0)
}


//: [Next](@next)
