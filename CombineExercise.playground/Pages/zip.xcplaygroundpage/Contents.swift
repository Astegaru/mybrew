//: [Previous](@previous)

import Foundation
import Combine

// Zip pair different type elements from publishers but it's not waiting for the latest one. It's picking and pairing depending on the values received

let publisher1 = PassthroughSubject<Int,Never>()
let publisher2 = PassthroughSubject<String,Never>()

publisher1.zip(publisher2)
    .sink {
        print($0.0, $0.1)
    }

publisher1.send(1)
publisher1.send(2)

publisher2.send("A")
publisher2.send("B")

// Zip waits for the publishers to emit the same ammount of values, good for using in network requests that wait for one another. That's why 3 is not printed...

publisher1.send(3)
publisher1.send(4)
// Uncomment bellow to print the 3 and C.

//publisher2.send("C")
//publisher2.send("D")
//: [Next](@next)
