//: [Previous](@previous)

import Foundation
import Combine

let publisher = (1...10).publisher

publisher.scan([]) { numbers, value -> [Int] in
numbers + [value]
}
.sink {
    print($0)
}
// Second ex

let publisher2 = (1...10).publisher
var shitNumber = 1
publisher2.scan(shitNumber) { number, value -> Int in
    number * value
}
.sink {
    print($0)
}

// Third shit

let pub3 = (1...10).publisher
pub3.scan(1) { number, factor -> Int in
    var factorial = 0
    factorial +=  number * factor
    return factorial
}
.sink {
    print($0)
}
// Forth shit

var pub4 = [1,2,3].publisher
pub4.scan([0]) { number, number2 -> [Int] in
    return number + [number2]
}
.sink {
    print($0)
}

// Fibonaci

func giveFibonacciSequence(_ upTo: Int) {
    var lowerValue: Int = 1
    var upperValue: Int = 1
    let publisher = (1...upTo).publisher
    publisher.scan([1]) { numbers, nextValue -> [Int] in
        let sumOfValues = numbers + [lowerValue + upperValue]
        lowerValue = upperValue
        upperValue = sumOfValues.last!
      return  sumOfValues
        
    }
    .sink {
        print($0)
    }

}
giveFibonacciSequence(13)

// MARK: - This has nothing to do with combine
func normalFibonacciSequence(_ upTo: Int) {
    var counter = 0
    var lowerValue = 1
    var upperValue = 1
    var sequence: [Int] = [lowerValue]
    while counter < upTo {
        counter += 1
        var z = lowerValue + upperValue
        lowerValue = upperValue
        upperValue = z
        sequence.append(z)
    }
    print(sequence)
}
normalFibonacciSequence(34)

func normalFibonacciSequence2(_ until: Int) {
    var lowerValue = 1
    var upperValue = 1
    var sequence: [Int] = [lowerValue]
    while (lowerValue + upperValue) < until {
        
        var z = lowerValue + upperValue
        lowerValue = upperValue
        upperValue = z
        sequence.append(z)
    }
    print(sequence)
}
normalFibonacciSequence2(50)
//: [Next](@next)
