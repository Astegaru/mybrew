//: [Previous](@previous)

import Foundation
import Combine

// finding different values (the first value/sequence of a particular value) as soon as it does that it will finish the sequence.

let numbers = (1...9).publisher
    .first(where: {$0 % 2 == 0})
    .sink { completion in
        switch completion {
        case .failure: break;
        case .finished:
            print("Finished looking for the first number.")
        }
    } receiveValue: {
        print($0)
    }


//: [Next](@next)
