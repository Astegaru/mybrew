//: [Previous](@previous)

import Foundation
import Combine

enum MyError: Error {
    case notAString, empty
    case other(Error)
    
    static func map(_ error: Error) -> MyError {
        return (error as? MyError) ?? .other(error)
    }
}

class StringSubscriber: Subscriber {
    func receive(subscription: Subscription) {
        print("Received subscription")
//        subscription.request(.max(3))
    }
    
    func receive(_ input: String) -> Subscribers.Demand {
        print("Received value: \(input)")
        return .none
    }
    
    func receive(completion: Subscribers.Completion<Never>) {
        print("Work completed")
    }

    
    typealias Input = String
    typealias Failure = Never
}

// Creating a publisher
let publisher = ["A","B","C","D","E","F","G"].publisher

// Creating our subscriber
let subscriber = StringSubscriber() 

// Announce the publisher that we have a new subscriber
publisher.subscribe(subscriber)


//: [Next](@next)
