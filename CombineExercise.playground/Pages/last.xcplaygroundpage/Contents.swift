//: [Previous](@previous)

import Foundation
import Combine
// FInds the last value in the sequence that satisfies a particular condition set by you
let numbers = (1...9).publisher
    .last(where: {$0 % 2 != 0})
    .sink { completion in
        switch completion {
        case .failure: break;
        case .finished:
            print("Finished looking for the first number.")
        }
    } receiveValue: {
        print($0)
    }

//: [Next](@next)
