//: [Previous](@previous)

import Foundation
import Combine

// MARK: - Without Collect
print("Without Collect: [\"A\",\"B\",\"C\",\"D\",\"E\",\"F\"].publisher.sink \n")
["A","B","C","D","E","F"].publisher.sink {
    print($0)
}

// MARK: - With Collect
print("\nWith Collect: [\"A\",\"B\",\"C\",\"D\",\"E\",\"F\"].publisher.collect().sink \n")
["A","B","C","D","E","F"].publisher.collect().sink {
    print($0)
}

// MARK: - With Collect + Arguments
print("\nWith Collect + Arguments: [\"A\",\"B\",\"C\",\"D\",\"E\",\"F\"].publisher.collect(3).sink \n")
["A","B","C","D","E","F"].publisher.collect(3).sink {
    print($0)
}

//: [Next]#fileLiteral(resourceName: "Published ex.xcplaygroundpage")(@next)
