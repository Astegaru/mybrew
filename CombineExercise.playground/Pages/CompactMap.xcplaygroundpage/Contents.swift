//: [Previous](@previous)

import Foundation
import Combine

// Compact map is used to remove the nils from a string

let strings = ["a","1.24","b","3.45","4.5"].publisher
    .compactMap{ Float($0) }
    .sink {
        print($0)
    }

//: [Next](@next)
