//: [Previous](@previous)

import Foundation
import Combine


let publisher = ["A","B","C","D"].publisher
publisher.first()
    .sink {
        print($0)
    }

publisher.first(where: { "Cat".contains($0)})
    .sink {
        print($0)
    }
//: [Next](@next)
