import UIKit
import Combine
import SwiftUI

// MARK: - Combine Method with Published
class Weather {
    @Published var temperature: Double
    init(temperature: Double) {
        self.temperature = temperature
    }
}
var cancellable: AnyCancellable?

let weather = Weather(temperature: 20)

cancellable = weather.$temperature
    .sink() {
        print ("Temperature now: \($0)")
    }
weather.temperature = 25
weather.temperature = 30

// MARK: - Property observers method
print("\n Observers way \n")
class Weather2 {
    @Published var temperature: Double {
        willSet {
//            print("\n\n\t\t This thing is about to be set up from \n\t CurrentValue: \(temperature) \n to \n\t NewValue: \(newValue) \n\n\t\t")
            
            print("Temperature now: \(temperature)")
//            if newValue != temperature {
//                print("Temperature now: \(newValue)")
//            }
            

        }
        didSet {
//            print("\n\n\t\t This thing has been set up from \n\t oldValue: \(oldValue) \n to \n\t NewValue: \(temperature) \n\n\t\t")
//            print("Temperature now: \(temperature)")
        }
    }
    init(temperature: Double) {
        self.temperature = temperature
    }
}


let weather2 = Weather2(temperature: 15)
//cancellable = weather2.$temperature.sink() {
//    print($0)
//}
weather2.temperature = 16
weather2.temperature = 17
//weather2.temperature = 18


// MARK: - Combine multicast, multicast<S>(_ createSubject: @escaping () -> S)
print("\n Combine multicast, multicast<S>(_ createSubject: @escaping () -> S) \n")
var cancellable1: AnyCancellable?
var cancellable2: AnyCancellable?

     let pub = ["First", "Second", "Third"].publisher
         .map( { return ($0, Int.random(in: 0...100)) } )
         .print("Random")
         .multicast { PassthroughSubject<(String, Int), Never>() }

     cancellable1 = pub
        .sink { print ("Stream 1 received: \($0)")}
     cancellable2 = pub
        .sink { print ("Stream 2 received: \($0)")}
     pub.connect()

// MARK: - Combine multicast, multicast<S>(subject: S)
print("\n Combine multicast, multicast<S>(subject: S) \n")

     let pub2 = ["First", "Second", "Third"].publisher
         .map( { return ($0, Int.random(in: 0...100)) } )
         .print("Random")
         .multicast(subject: PassthroughSubject<(String, Int), Never>())

     cancellable1 = pub2
         .sink { print ("Stream 1 received: \($0)")}
     cancellable2 = pub2
         .sink { print ("Stream 2 received: \($0)")}
     pub.connect()


// MARK: - Easier example of how combine solves things

print("\n Without Combine \n")
var userBalance = 5
let productPrice = 10

let canMakePurchase = userBalance >= productPrice

print(canMakePurchase) // this here is oviously FALSE.

userBalance += 20

print(canMakePurchase) // even though we increased the userBalance this is still FALSE because canMakeUserPurchase was not updated with the latest ballance
print("\n With Combine \n")

var userBalanceB = Just(25)
var priceForItem = Just(35)

let canPurchase = priceForItem.combineLatest(userBalanceB)
    .map { $0 >= $1 }


print(canPurchase)


