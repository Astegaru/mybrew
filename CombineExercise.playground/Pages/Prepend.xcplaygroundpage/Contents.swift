//: [Previous](@previous)

import Foundation
import Combine

// Appends your sequence before the initial values
let publisher2 = (102...150).publisher
let numbers = (1...5).publisher
    .prepend(100,101)
    .prepend(-1,-2)
    .prepend([-100,-75])
    .prepend(publisher2)
    .sink {
        print($0)// Don;t Print
    }

//: [Next](@next)
