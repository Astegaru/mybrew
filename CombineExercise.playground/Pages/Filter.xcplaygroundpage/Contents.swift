//: [Previous](@previous)

import Foundation
import Combine

let numbers = (1...20).publisher

//Evaluates the return condition (BOOL) and then filters out the elements that do not corespond ot the filter
numbers.filter {
    $0 % 2 == 0
}
.sink {
    print($0)
}
numbers.filter{
    $0 % 2 != 0
}
.sink {
    print($0)
}
//: [Next](@next)
