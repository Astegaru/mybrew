//: [Previous](@previous)

import Foundation
import Combine

class BoolSubscriber: Subscriber {
    func receive(subscription: Subscription) {
        subscription.request(.max(1))
    }
    
    func receive(_ input: Bool) -> Subscribers.Demand {
        print("Bool Subscriber set to: \(input)")
        return .max(1)
    }
    
    func receive(completion: Subscribers.Completion<Never>) {
        print("Completion")
    }
    
    typealias Input = Bool
    typealias Failure = Never
}

let stateSubscriber = BoolSubscriber()
var state = CurrentValueSubject<Bool,Never>(false)
state.subscribe(stateSubscriber)
print(state.value)
state.send(true)
print(state.value)
state.value = false
print(state.value)

let secondSubscriber = state.sink { val in
    print("State has change value from: \(state.value)  to: \(val)")
}

state.send(false)
state.send(false)
state.send(false)
state.send(true)


//With PassThrough
print("\n Passthrough differences \n")
var pass = PassthroughSubject<Bool,Never>()
let boolsub = BoolSubscriber()
pass.subscribe(boolsub)
let secondPassSub = pass.sink { val in
    print("Value set to: \(val)")
}
pass.send(false)
pass.send(false)
//: [Next](@next)
