//: [Previous](@previous)

import UIKit
import Combine
import Foundation

// 1. Creating notification
let notification = Notification.Name("SomeNotification")
// 2. Creating a publisher for notification center that we can subsribe to later on
let publisher = NotificationCenter.default.publisher(for: notification, object: nil)
// 3. Create notification center post in order to post the notificaiton. This wont get sent because we are posting before having a subscriber to our publisher, we will post it again on step 5
NotificationCenter.default.post(name: notification, object: nil)
// 4. Create a subscription based on the publisher we created at step 2 using sink. The sink basically saying that we want to subscribe to this particular publisher.
let subscription = publisher.sink { _ in
    print("Notificaiton Received")
}
    
// 5. Creating another post from our Notificaiton Center after adding a subscription to our publisher in order to see the print from sink
NotificationCenter.default.post(name: notification, object: nil)


// MARK: - Notification Done better

extension Notification.Name {
    static let newBlogPost = Notification.Name("new_blog_post")
}

struct BlogPost {
    let title: String
    let url: URL
}

let blogPostPublisher = NotificationCenter.Publisher(center: .default, name: .newBlogPost, object: nil)
    .map { (notification) -> String? in
        return (notification.object as? BlogPost)?.title ?? "No valid title in the notification body"
    }
let lastPostLable = UILabel()
//let lastPostLabelSubscriber = Subscribers.Assign(object: lastPostLable, keyPath: \.text)
//blogPostPublisher
//    .subscribe(lastPostLabelSubscriber)

// The above is creating a subscriber directly, but the assign soperator subscribes to the notification publisher
blogPostPublisher.assign(to: \.text, on: lastPostLable)


let blogPost = BlogPost(title: "Getting started with Combine", url: URL(string: "https://www.avanderlee.com/swift/combine/")!)


NotificationCenter.default.post(name: .newBlogPost, object: blogPost)
print("Last Post is: \(lastPostLable.text!)")
let blogPost2 = BlogPost(title: "Another message", url: URL(string: "https://www.avanderlee.com/swift/combine/")!)
NotificationCenter.default.post(name: .newBlogPost, object: blogPost2)
print("Last Post is: \(lastPostLable.text!)")
NotificationCenter.default.post(name: .newBlogPost, object: "nay")
print("Last Post is: \(lastPostLable.text!)")

//: [Next](@next)
