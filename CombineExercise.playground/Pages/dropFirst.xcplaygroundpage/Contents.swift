//: [Previous](@previous)

import Foundation
import Combine

// Allows you to drop certain values from the sequence

let numbers = (1...10).publisher
    .dropFirst(8)
    .sink {
        print($0)
    }
    

//: [Next](@next)
