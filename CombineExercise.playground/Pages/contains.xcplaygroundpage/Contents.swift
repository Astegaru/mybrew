//: [Previous](@previous)

import Foundation
import Combine

//: [Next](@next)

let publisher = ["A","B","C","D"].publisher

publisher.contains("C")
    .sink {
        print($0)
    }
