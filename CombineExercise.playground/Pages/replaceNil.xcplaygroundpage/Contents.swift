//: [Previous](@previous)

import Foundation
import Combine

["A","B","C","D",nil,"E"].publisher
    .replaceNil(with: "*")
    .map { someValue -> String in
        return someValue
    }
    .sink {
        print($0)
    }

//: [Next](@next)
