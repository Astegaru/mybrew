//: [Previous](@previous)

import Foundation
import Combine

//Ignore output, it's simply going to ignore the output and going to emit the completion event when it's done

let numbers = (0...5000).publisher
//    .ignoreOutput()
    .sink { closure in
        switch closure {
        case .failure: break
        case .finished:
            print("We finished running the loop, this is Output")
        }
    } receiveValue: {_ in
//        print($0)
    }


//: [Next](@next)
