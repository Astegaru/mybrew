//: [Previous](@previous)

import Foundation
import Combine

enum MyError: Error {
    case subscriberError
    case notAString, empty
    case other(Error)
    
    static func map(_ error: Error) -> MyError {
        return (error as? MyError) ?? .other(error)
    }
}

class StringSubscriber: Subscriber {
    func receive(subscription: Subscription) {
        subscription.request(.max(8))
    }
    
    func receive(_ input: String) -> Subscribers.Demand {
        print(input)
        return .none
    }
    
    func receive(completion: Subscribers.Completion<MyError>) {
        print(completion)
    }
    
    typealias Input = String
    typealias Failure = MyError
}

// Define a subject with the type of Input: String and Error: MyError
let subject = PassthroughSubject<String, MyError>()
// Create subscriber
let subscriber = StringSubscriber()

// Add a subscriber to the subject
subject.subscribe(subscriber)
// Send a value to the subscriber
subject.send("A")

subject.send("")

subject.send("B")

subject.send("C")

subject.send("D")
// Publisher is both a pubisher and subscriber so how do we create a subscription

let secondSubscriber = subject.sink { (completion) in
    print("Received completion from sink")
} receiveValue: { string in
    print("Received value from sink: \(string)")
}

// Sending some more values so the subscription(subscriber) from sink also gets something published

subject.send("E")

subject.send("F")
secondSubscriber.cancel()
subject.send("G")

//: [Next](@next)
