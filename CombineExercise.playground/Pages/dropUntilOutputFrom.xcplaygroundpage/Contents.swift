//: [Previous](@previous)

import Foundation
import Combine

// Drop until, drop values from sequence untill it gets an output from other publisher

let isReady = PassthroughSubject<Void,Never>()
let taps = PassthroughSubject<Int,Never>()
    
    taps.drop(untilOutputFrom: isReady).sink {
        print($0)
    }

(1...10).forEach { n in
    taps.send(n)
    if n == 3 {
        isReady.send()
    }
}

//: [Next](@next)
