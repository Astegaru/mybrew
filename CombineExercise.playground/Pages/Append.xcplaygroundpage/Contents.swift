//: [Previous](@previous)

import Foundation
import Combine

let numbers = (1...10).publisher
let publisher2 = (100...110).publisher

numbers
    .append(11,12)
    .append(publisher2)
    .sink {
    print($0)
}



//: [Next](@next)
