//: [Previous](@previous)

import Foundation
import Combine

// Sequence operators they usually work on an arrays or a set

let publisher = [1,-45,100,234,-24,-158,365].publisher

publisher.min()
    .sink {
        print($0)
    }

//: [Next](@next)
