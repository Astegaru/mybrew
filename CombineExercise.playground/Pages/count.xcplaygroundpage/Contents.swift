//: [Previous](@previous)

import Foundation
import Combine

let publisher = ["A","B","C","D"].publisher

publisher.count() // it's a just
    .sink {
        print($0)
    }


let publisher1 = PassthroughSubject<Int, Never>()
publisher1.count()
    .sink {
        print($0)
    }
// Get the finishing event
publisher1.send(10)
publisher1.send(10)
publisher1.send(10)
publisher1.send(completion: .finished)
//: [Next](@next)
