//: [Previous](@previous)

import Foundation
import Combine

// Get's a sequence and reduces it down to a single particular value.
let publisher = ["A","B","C","D"].publisher

publisher
    .reduce("", { accumulator, value in
        accumulator + value
    })
    .sink {
        print($0)
    }


//: [Next](@next)
